﻿using UnityEngine;
using System.Collections;

namespace Project2
{
    public class Ball : MonoBehaviour
    {
        public BallType Type;

        public Rigidbody2D BallRigidbody;

        public SpriteRenderer BallSprite;

        bool _BallReconfigured = false;

        // Inicjalizuje sobie obiekt, 
        public void Init(bool forceBlue)
        {
            if (!forceBlue)
            {
                int typeRandom = Random.Range(0, 100) > 50 ? 0 : 1;

                switch (typeRandom)
                {
                    case 0:
                        ConfigureBall(BallType.Red);
                        break;
                    case 1:
                        ConfigureBall(BallType.Yellow);
                        break;
                }
            }
            else
            {
                ConfigureBall(BallType.Blue);
            }
        }

        void FixedUpdate()
        {
            UpdateMovement();
        }

        void UpdateMovement()
        {
            if(!_BallReconfigured)
            {
                if(SpawnManager.Instance.BallCount >= 300)
                {
                    _BallReconfigured = true;
                    ConfigureBall(BallType.Blue);
                }
            }

            Vector2 randomVector = Random.insideUnitCircle * 2.0f;

            if (Type == BallType.Blue)
            {
                Vector3 mouseToWorld    = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                randomVector            = (mouseToWorld - transform.position) * 1.5f;
            }

            BallRigidbody.AddForce(randomVector * Time.fixedDeltaTime);
        }

        void ConfigureBall(BallType type)
        {
            switch (type)
            {
                case BallType.Red:
                    Type = BallType.Red;
                    gameObject.layer = LayerMask.NameToLayer("RedBall");
                    BallSprite.color = Color.red;
                    break;
                case BallType.Yellow:
                    Type = BallType.Yellow;
                    gameObject.layer = LayerMask.NameToLayer("YellowBall");
                    BallSprite.color = Color.yellow;
                    break;
                case BallType.Blue:
                    Type = BallType.Blue;
                    gameObject.layer = LayerMask.NameToLayer("BlueBall");
                    BallSprite.color = Color.blue;
                    break;
            }
        }
    }

    public enum BallType
    {
        Red,
        Yellow,
        Blue
    }
}

