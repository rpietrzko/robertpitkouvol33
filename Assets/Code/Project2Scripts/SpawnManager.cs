﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Project2;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance;

    public GameObject   BallPrefab;
    public Transform    BallParent;

    public Text BallCountText;

    public int BallCount = 0;

    int _MaxBallCount = 600;

    IEnumerator SpawningCoroutine;

    void Awake()
    {
        Instance = this;
    }

	void Start ()
    {
        SpawningCoroutine = SpawnBallsr();

        StartCoroutine(SpawningCoroutine);

        UpdateUI();
    }

    IEnumerator SpawnBallsr()
    {
        float ballSpawnPauseTime = 0.1f;

        BallCount = 0;

        while(BallCount < _MaxBallCount)
        {
            SpawnBall();

            yield return new WaitForSeconds(ballSpawnPauseTime);
        }

        yield return 0;
    }

    void SpawnBall()
    {
        BallCount++;

        UpdateUI();

        Vector2 newBallPosition = new Vector2(0.0f, 0.0f);

        GameObject newBallGO = Object.Instantiate(BallPrefab, newBallPosition, Quaternion.identity) as GameObject;

        newBallGO.transform.parent = BallParent;

        Ball newBall = newBallGO.GetComponent<Ball>();

        bool forceBlue = BallCount >= 300;

        newBall.Init(forceBlue);
    }

    void UpdateUI()
    {
        BallCountText.text = "Ball Count: " + BallCount;
    }
}
