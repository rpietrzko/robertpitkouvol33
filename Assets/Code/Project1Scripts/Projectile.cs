﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    Rigidbody2D _ProjectileRigidbody;

    SpringJoint2D _Joint;

    SlingManager _Sling;

    LineRenderer _SlingFrontLine;
    LineRenderer _SlingBackLine;

    Ray _RayToMouse;
    Ray _LeftCatapultToProjectile;

    float _MatchStretchSqf = 0.0f;
    float _ColliderRadius;

    bool _MouseButtonPressed    = false;
    bool _JointDetroyed         = false;

    Vector2 _PreviousVelocity = Vector2.zero;

    void Awake()
    {
        _Joint                  = gameObject.GetComponent<SpringJoint2D>();
        _ProjectileRigidbody    = gameObject.GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        _Sling = SlingManager.Instance;

        LineRendererSetup();

        _RayToMouse                 = new Ray(_Sling.transform.position, Vector3.zero);
        _LeftCatapultToProjectile   = new Ray(_SlingFrontLine.transform.position, Vector3.zero);

        _MatchStretchSqf    = _Sling.MaxStretch * _Sling.MaxStretch;

        _ColliderRadius     = gameObject.GetComponent<CircleCollider2D>().radius;
    }

    void FixedUpdate()
    {
        UpdateInput();
        UpdateLogic();
    }

    void UpdateInput()
    {
        if (_MouseButtonPressed)
            UpdateDragging();
    }

    void UpdateLogic()
    {
        if (!_JointDetroyed)
        {
            _Joint.distance = _Sling.MinStretch;

            if (!_ProjectileRigidbody.isKinematic && _PreviousVelocity.sqrMagnitude > _ProjectileRigidbody.velocity.sqrMagnitude)
            {
                DestroyJoint();
                _ProjectileRigidbody.velocity = _PreviousVelocity;
            }

            if (!_MouseButtonPressed)
                _PreviousVelocity = _ProjectileRigidbody.velocity;

            LineRendererUpdate();
        }
        else
        {
            _SlingFrontLine.enabled = false;
            _SlingBackLine.enabled  = false;
        }
    }

    void LineRendererSetup()
    {
        // 0 - front, 1 - back

        _SlingFrontLine = _Sling.SlingLines[0];
        _SlingBackLine  = _Sling.SlingLines[1];

        _SlingFrontLine.SetPosition(0,  _SlingFrontLine.transform.position);
        _SlingBackLine.SetPosition(0,   _SlingBackLine.transform.position);

        _SlingFrontLine.sortingLayerName    = "Foreground";
        _SlingBackLine.sortingLayerName     = "Foreground";

        _SlingFrontLine.sortingOrder    = 3;
        _SlingBackLine.sortingOrder     = 1;
    }

    void OnMouseDown()
    {
        _Joint.enabled      = false;
        _MouseButtonPressed = true;
    }

    void OnMouseUp()
    {
        _Joint.enabled                      = true;
        _ProjectileRigidbody.isKinematic    = false;
        _MouseButtonPressed                 = false;
    }

    void UpdateDragging()
    {
        Vector3 mouseToWorld    = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 dirSlingToMouse = mouseToWorld - _Sling.transform.position;

        if (dirSlingToMouse.sqrMagnitude > _MatchStretchSqf)
        {
            _RayToMouse.direction   = dirSlingToMouse;
            mouseToWorld            = _RayToMouse.GetPoint(_Sling.MaxStretch);
        }

        mouseToWorld.z      = 0f;
        transform.position  = mouseToWorld;
    }

    void LineRendererUpdate()
    {
        Vector2 dirSlingToProjectile        = transform.position - _SlingFrontLine.transform.position;
        _LeftCatapultToProjectile.direction = dirSlingToProjectile;

        Vector3 holdPoint = _LeftCatapultToProjectile.GetPoint(dirSlingToProjectile.magnitude + _ColliderRadius);
        _SlingFrontLine.SetPosition(1, holdPoint);
        _SlingBackLine.SetPosition(1, holdPoint);
    }

    void DestroyJoint()
    {
        _JointDetroyed = true;
        Destroy(_Joint);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground")
            SlingManager.Instance.Reset();
    }

    public SpringJoint2D GetJoint() { return _Joint; }
}
