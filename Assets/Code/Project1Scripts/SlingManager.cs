﻿using UnityEngine;
using System.Collections;

public class SlingManager : MonoBehaviour
{
    public static SlingManager Instance;

    public LineRenderer[] SlingLines;

    public float MinStretch = 1.0f;
    public float MaxStretch = 3.0f;

    void Awake()
    {
        Instance = this;
    }

    public void Reset()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
